Eliscripts
==========

Elixir stuff...


echoback.ex
-----------

This process will simply respond what you told it.
```
$ iex echoback.ex
Erlang/OTP 17 [erts-6.2] [source] [64-bit] [async-threads:10]

Interactive Elixir (1.0.0) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> pid = spawn(Echo, :back, [])
#PID<0.60.0>
iex(2)> send(pid, {self(), :Hello}) 
Hey, did you just say Hello?
{#PID<0.58.0>, :Hello}
iex(3)> flush
{"Hey, did you just say 'Hello' to me?"}
:ok
```

So, basically we first get the PID of the process we launched. In this context, spawn takes the Module, the function and its arguments in a list as parameters.  
Next, we send a message to that process, using `#{pid}`. The message itself is composed of `self()` (which is our current PID) and `:Hello` which an atom 
(that is to say it's nothing more than its own value).
The echoback server replies with `IO.puts` **AND** `send()` because we sent it our PID in the `send()` request. We can see we received a message using the
`flush` function.
