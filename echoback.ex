defmodule Echo do

  def back do
    receive do
      {pid, message} ->
       IO.puts("Received #{message} from #{pid}")
       send(pid, {"Hey, did you just say '#{message}' to me?"})
    end # receive
    back()
  end # back
end # defmodule
